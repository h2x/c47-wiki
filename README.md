# C47 wiki

This project is a container for the [C47 calculator community wiki pages](https://gitlab.com/h2x/c47-wiki/-/wikis/home).

In order to request access, please reach out to me or one of the other C47 team members on the Swiss Micros forum.
